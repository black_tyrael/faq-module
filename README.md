# Faq Module

F.A.Q Module for BlizzCMS-Plus
This is a adaptation of BlizzCMS-Dev module.

# Requirements

**_BlizzCMS Plus_** 1.0.4

# Installation

You must place the files in the corresponding folders.

The "faq" folder must go inside the Modules folder.

Next paste the content of the language and path files in their respective places. This to show the text in different languages, English and Spanish. And you can access the Faq module through your website with friendly URL.

Files to open to paste:

application/config/route.php
application/language/english/general_lang.php
application/language/spanish/general_lang.php

Paste the content of the files located in the folders we provide.

--- SQL ---

Run the SQL files that we have left in the folder and configure your FAQ's.
We have left an example of how it would look.

FAQs must be added by SQL.

Now go to your website and click on the link/faq.
It should look something like this: https: // wow-cms/en/faq

Enjoy.

## Copyright

Copyright © 2020 [WoW-CMS](https://wow-cms.com).
